<?php
namespace Kanboard\WebBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class KanboardWebBundle
 * @package Kanboard\WebBundle
 */
class KanboardWebBundle extends Bundle {

}
