<?php

namespace Kanboard\WebBundle\Form;

use Kanboard\WebBundle\Validator\Constraint\UserEmail;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class UserBoardType
 * @package Kanboard\WebBundle\Form
 */
class UserBoardType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('email', EmailType::class, [
                'constraints' => [
                    new Email(),
                    new NotBlank(),
                    new UserEmail()
                ]
            ])
            ->add('add', SubmitType::class);
    }
}