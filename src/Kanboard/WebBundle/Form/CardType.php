<?php

namespace Kanboard\WebBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CardType
 * @package Kanboard\WebBundle\Form
 */
class CardType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $boardMembers = $options['boardMembers'];

        $builder
            ->add('name', TextType::class)
            ->add('description', TextareaType::class)
            ->add('dueDate', DateType::class)
            ->add('estimateTime', TextType::class, [
                'attr' => [
                    'placeholder' => '1d 2h 3m'
                ]
            ])
            ->add('spentTime', TextType::class, [
                'attr' => [
                    'placeholder' => '1d 2h 3m'
                ]
            ])
            ->add('remainingTime', TextType::class, [
                'attr' => [
                    'placeholder' => '1d 2h 3m'
                ]
            ])
            ->add('members', EntityType::class, [
                'class' => 'KanboardWebBundle:User',
                'choice_label' => 'email',
                'multiple' => true,
                'expanded' => true,
                'choices' => $boardMembers
            ])
            ->add('save', SubmitType::class);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => 'Kanboard\WebBundle\Entity\Card',
            'boardMembers' => null
        ]);
    }
}