<?php
namespace Kanboard\WebBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Board
 *
 * @ORM\Table("boards")
 * @ORM\Entity(repositoryClass="Kanboard\WebBundle\Entity\BoardRepository")
 */
class Board {

    /**
     * @var integer

     * @ORM\Id
     * @ORM\Column(name="id", type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="creator_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $creator;

    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="boards")
     * @ORM\JoinTable(name="users_boards")
     */
    private $members;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_date", type="integer")
     */
    private $createdDate;

    /**
     * Board constructor.
     * @param User $creator
     * @param string $name
     */
    public function __construct(User $creator, $name = '') {
        $this->creator = $creator;
        $this->name = $name;
        $this->createdDate = time();
        $this->members = new ArrayCollection([$creator]);
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * @return User
     */
    public function getCreator() {
        return $this->creator;
    }

    /**
     * @param User $creator
     */
    public function setCreator($creator) {
        $this->creator = $creator;
    }

    /**
     * @return int
     */
    public function getCreatedDate() {
        return $this->createdDate;
    }

    /**
     * @param int $createdDate
     */
    public function setCreatedDate($createdDate) {
        $this->createdDate = $createdDate;
    }

    /**
     * @return mixed
     */
    public function getMembers() {
        return $this->members;
    }

    /**
     * @param mixed $members
     */
    public function setMembers($members) {
        $this->members = $members;
    }

}