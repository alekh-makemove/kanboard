<?php

namespace Kanboard\WebBundle\Entity;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table("users")
 * @ORM\Entity(repositoryClass="Kanboard\WebBundle\Entity\UserRepository")
 * @UniqueEntity(fields="email", message="Email already taken")
 */
class User implements UserInterface {

    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max=4096)
     */
    private $plainPassword;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Board", mappedBy="members")
     */
    private $boards;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Card", mappedBy="members")
     */
    private $cards;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_date", type="integer")
     */
    private $createdDate;

    /**
     * User constructor.
     */
    public function __construct() {
        $this->createdDate = time();
        $this->boards = new ArrayCollection();
    }

    /**
     * @return array
     */
    public function getRoles() {
        return ['ROLE_USER'];
    }

    /**
     * @return string
     */
    public function getSalt() {
        return '';
    }

    /**
     * @return string
     */
    public function getUsername() {
        return $this->email;
    }

    /**
     *
     */
    public function eraseCredentials() {

    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email) {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password) {
        $this->password = $password;
    }

    /**
     * @return int
     */
    public function getCreatedDate() {
        return $this->createdDate;
    }

    /**
     * @param int $createdDate
     */
    public function setCreatedDate($createdDate) {
        $this->createdDate = $createdDate;
    }

    /**
     * @return mixed
     */
    public function getPlainPassword() {
        return $this->plainPassword;
    }

    /**
     * @param mixed $plainPassword
     */
    public function setPlainPassword($plainPassword) {
        $this->plainPassword = $plainPassword;
    }

    /**
     * @return mixed
     */
    public function getBoards() {
        return $this->boards;
    }

    /**
     * @param mixed $boards
     */
    public function setBoards($boards) {
        $this->boards = $boards;
    }

    /**
     * @return ArrayCollection
     */
    public function getCards() {
        return $this->cards;
    }

    /**
     * @param ArrayCollection $cards
     */
    public function setCards($cards) {
        $this->cards = $cards;
    }

}