<?php
namespace Kanboard\WebBundle\Entity;

/**
 * Class ColumnRepository
 * @package Kanboard\WebBundle\Entity
 */
class ColumnRepository extends BaseRepository {

    /**
     * @param Column $column
     */
    public function save(Column $column) {
        $this->saveEntity($column);
    }

    /**
     * @param Column $column
     */
    public function remove(Column $column) {
        $this->removeEntity($column);
    }

}
