<?php

namespace Kanboard\WebBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * CardComment
 *
 * @ORM\Table("cards_comments")
 * @ORM\Entity()
 */
class CardComment {

    /**
     * @var integer

     * @ORM\Id
     * @ORM\Column(name="id", type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * @var Card
     *
     * @ORM\ManyToOne(targetEntity="Card")
     * @ORM\JoinColumn(name="card_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $card;

    /**
     * @var String
     *
     * @ORM\Column(name="comment", type="text")
     * @Assert\NotBlank()
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_date", type="integer")
     * @Assert\NotBlank()
     */
    private $createdDate;

    /**
     * CardComment constructor.
     * @param User $user
     * @param Card $card
     */
    public function __construct(User $user, Card $card) {
        $this->createdDate = time();
        $this->user = $user;
        $this->card = $card;
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return User
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user) {
        $this->user = $user;
    }

    /**
     * @return Card
     */
    public function getCard() {
        return $this->card;
    }

    /**
     * @param Card $card
     */
    public function setCard($card) {
        $this->card = $card;
    }

    /**
     * @return String
     */
    public function getComment() {
        return $this->comment;
    }

    /**
     * @param String $comment
     */
    public function setComment($comment) {
        $this->comment = $comment;
    }

    /**
     * @return int
     */
    public function getCreatedDate() {
        return $this->createdDate;
    }

    /**
     * @param int $createdDate
     */
    public function setCreatedDate($createdDate) {
        $this->createdDate = $createdDate;
    }

}