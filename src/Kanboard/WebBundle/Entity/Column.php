<?php

namespace Kanboard\WebBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Column
 *
 * @ORM\Table("columns")
 * @ORM\Entity(repositoryClass="Kanboard\WebBundle\Entity\ColumnRepository")
 */
class Column {

    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Board")
     * @ORM\JoinColumn(name="board_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $board;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_date", type="integer")
     */
    private $createdDate;

    /**
     * @ORM\OneToMany(targetEntity="Card", mappedBy="column")
     */
    private $cards;

    /**
     * Column constructor.
     * @param Board $board
     * @param string $name
     */
    public function __construct(Board $board, $name = '') {
        $this->board = $board;
        $this->name = $name;
        $this->createdDate = time();
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * @return User
     */
    public function getBoard() {
        return $this->board;
    }

    /**
     * @param User $board
     */
    public function setBoard($board) {
        $this->board = $board;
    }

    /**
     * @return int
     */
    public function getCreatedDate() {
        return $this->createdDate;
    }

    /**
     * @param int $createdDate
     */
    public function setCreatedDate($createdDate) {
        $this->createdDate = $createdDate;
    }

    /**
     * @return mixed
     */
    public function getCards() {
        return $this->cards;
    }

    /**
     * @param mixed $cards
     */
    public function setCards($cards) {
        $this->cards = $cards;
    }

}