<?php
namespace Kanboard\WebBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Class BaseRepository
 * @package Kanboard\WebBundle\Entity
 */
class BaseRepository extends EntityRepository {

    /**
     * @param $entity
     */
    public function saveEntity($entity) {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }

    /**
     * @param $entity
     */
    public function removeEntity($entity) {
        $em = $this->getEntityManager();
        $em->remove($entity);
        $em->flush();
    }

}
