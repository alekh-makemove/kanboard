<?php
namespace Kanboard\WebBundle\Entity;

/**
 * Class UserRepository
 * @package Kanboard\WebBundle\Entity
 */
class UserRepository extends BaseRepository {

    /**
     * @param User $user
     */
    public function save(User $user) {
        $this->saveEntity($user);
    }

    /**
     * @param User $user
     */
    public function remove(User $user) {
        $this->removeEntity($user);
    }

    /**
     * @param $email
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findUserByEmail($email) {
        return $this
            ->getEntityManager()
            ->createQuery('SELECT u FROM KanboardWebBundle:User u WHERE u.email = :email')
            ->setParameter('email', $email)
            ->getOneOrNullResult();
    }

}
