<?php
namespace Kanboard\WebBundle\Entity;

/**
 * Class CardRepository
 * @package Kanboard\WebBundle\Entity
 */
class CardRepository extends BaseRepository {

    /**
     * @param Card $card
     */
    public function save(Card $card) {
        $this->saveEntity($card);
    }

    /**
     * @param Card $card
     */
    public function remove(Card $card) {
        $this->removeEntity($card);
    }

    /**
     * @param CardComment $comment
     */
    public function saveComment(CardComment $comment) {
        $this->saveEntity($comment);
    }

    /**
     * @param CardComment $comment
     */
    public function removeComment(CardComment $comment) {
        $this->removeEntity($comment);
    }

}
