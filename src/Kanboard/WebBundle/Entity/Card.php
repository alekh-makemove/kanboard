<?php

namespace Kanboard\WebBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Kanboard\WebBundle\Helper\StringUtils;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Kanboard\WebBundle\Helper\TimeTranslatorHelper as TT;

/**
 * Card
 *
 * @ORM\Table("cards")
 * @ORM\Entity(repositoryClass="Kanboard\WebBundle\Entity\CardRepository")
 */
class Card {

    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Column
     *
     * @ORM\ManyToOne(targetEntity="Column")
     * @ORM\JoinColumn(name="column_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $column;

    /**
     * @var String
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var String
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dueDate", type="date", nullable=true)
     */
    private $dueDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="estimateTime", type="integer", length=32, nullable=true)
     */
    private $estimateTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="remainingTime", type="integer", length=32, nullable=true)
     */
    private $remainingTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="spentTime", type="integer", length=32, nullable=true)
     */
    private $spentTime;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="User", inversedBy="cards")
     * @ORM\JoinTable(name="users_cards")
     */
    private $members;

    /**
     * @ORM\OneToMany(targetEntity="CardComment", mappedBy="card")
     */
    private $comments;

    /**
     * @ORM\OneToMany(targetEntity="Checklist", mappedBy="card")
     */
    private $checklists;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_date", type="integer")
     * @Assert\NotBlank()
     */
    private $createdDate;

    /**
     * Card constructor.
     * @param Column $column
     */
    public function __construct(Column $column) {
        $this->dueDate = new \DateTime();
        $this->column = $column;
        $this->createdDate = time();
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return Column
     */
    public function getColumn() {
        return $this->column;
    }

    /**
     * @param Column $column
     */
    public function setColumn($column) {
        $this->column = $column;
    }

    /**
     * @return String
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param String $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * @return String
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @param String $description
     */
    public function setDescription($description) {
        $this->description = $description;
    }

    /**
     * @return \DateTime
     */
    public function getDueDate() {
        return $this->dueDate;
    }

    /**
     * @param \DateTime $dueDate
     */
    public function setDueDate($dueDate) {
        $this->dueDate = $dueDate;
    }

    /**
     * @return int
     */
    public function getEstimateTime() {
        return TT::timeToString($this->estimateTime);
    }

    /**
     * @return int
     */
    public function getEstimateTimeInMinutes() {
        return $this->estimateTime;
    }

    /**
     * @param string $estimateTime
     */
    public function setEstimateTime($estimateTime) {
        $this->estimateTime = TT::timeToMinutes($estimateTime);
    }

    /**
     * @return int
     */
    public function getSpentTime() {
        return TT::timeToString($this->spentTime);
    }

    /**
     * @return int
     */
    public function getSpentTimeInMinutes() {
        return $this->spentTime;
    }

    /**
     * @param int $spentTime
     */
    public function setSpentTime($spentTime) {
        // Convert string like '1d 13h 24m' to minutes
        $this->spentTime = TT::timeToMinutes($spentTime);
    }

    /**
     * @return int
     */
    public function getRemainingTime() {
        // Convert minutes to string like '1d 13h 24m'
        return TT::timeToString($this->remainingTime);
    }

    /**
     * @return int
     */
    public function getRemainingTimeInMinutes() {
        return $this->remainingTime;
    }

    /**
     * @param int $remainingTime
     */
    public function setRemainingTime($remainingTime) {
        if (!StringUtils::isBlankOrNull($remainingTime)) {
            $this->remainingTime = TT::timeToMinutes($remainingTime);
        } else {
            $this->remainingTime = $this->estimateTime - $this->spentTime;
        }
    }

    /**
     * @return mixed
     */
    public function getMembers() {
        return $this->members;
    }

    /**
     * @param mixed $members
     */
    public function setMembers($members) {
        $this->members = $members;
    }

    /**
     * @return int
     */
    public function getCreatedDate() {
        return $this->createdDate;
    }

    /**
     * @param int $createdDate
     */
    public function setCreatedDate($createdDate) {
        $this->createdDate = $createdDate;
    }

    /**
     * @return mixed
     */
    public function getComments() {
        return $this->comments;
    }

    /**
     * @param mixed $comments
     */
    public function setComments($comments) {
        $this->comments = $comments;
    }

    /**
     * @return mixed
     */
    public function getChecklists() {
        return $this->checklists;
    }

    /**
     * @param mixed $checklists
     */
    public function setChecklists($checklists) {
        $this->checklists = $checklists;
    }

}