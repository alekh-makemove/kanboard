<?php

namespace Kanboard\WebBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Checklist
 *
 * @ORM\Table("checklists")
 * @ORM\Entity(repositoryClass="Kanboard\WebBundle\Entity\ChecklistRepository")
 */
class Checklist {

    /**
     * @var integer

     * @ORM\Id
     * @ORM\Column(name="id", type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Card
     *
     * @ORM\ManyToOne(targetEntity="Card")
     * @ORM\JoinColumn(name="card_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $card;

    /**
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_date", type="integer")
     * @Assert\NotBlank()
     */
    private $createdDate;

    /**
     * @ORM\OneToMany(targetEntity="ChecklistItem", mappedBy="checklist")
     */
    private $items;

    /**
     * Checklist constructor.
     * @param User $user
     * @param Card $card
     */
    public function __construct(User $user, Card $card) {
        $this->createdDate = time();
        $this->card = $card;
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return Card
     */
    public function getCard() {
        return $this->card;
    }

    /**
     * @param Card $card
     */
    public function setCard($card) {
        $this->card = $card;
    }

    /**
     * @return mixed
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getCreatedDate() {
        return $this->createdDate;
    }

    /**
     * @param int $createdDate
     */
    public function setCreatedDate($createdDate) {
        $this->createdDate = $createdDate;
    }

    /**
     * @return mixed
     */
    public function getItems() {
        return $this->items;
    }

    /**
     * @param mixed $items
     */
    public function setItems($items) {
        $this->items = $items;
    }

}