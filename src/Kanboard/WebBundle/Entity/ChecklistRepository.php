<?php
namespace Kanboard\WebBundle\Entity;

/**
 * Class ChecklistRepository
 * @package Kanboard\WebBundle\Entity
 */
class ChecklistRepository extends BaseRepository {

    /**
     * @param Checklist $checklist
     */
    public function save(Checklist $checklist) {
        $this->saveEntity($checklist);
    }

    /**
     * @param Checklist $checklist
     */
    public function remove(Checklist $checklist) {
        $this->removeEntity($checklist);
    }

    /**
     * @param ChecklistItem $checklistItem
     */
    public function saveChecklistItem(ChecklistItem $checklistItem) {
        $this->saveEntity($checklistItem);
    }

    /**
     * @param ChecklistItem $checklistItem
     */
    public function removeChecklistItem(ChecklistItem $checklistItem) {
        $this->removeEntity($checklistItem);
    }

    /**
     * @param $checklistId
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getChecklistItem($checklistId) {
        $dql = "SELECT ci FROM KanboardWebBundle:ChecklistItem ci WHERE ci.id = :id";

        return $this
            ->getEntityManager()
            ->createQuery($dql)
            ->setParameter('id', $checklistId)
            ->getOneOrNullResult();
    }

}
