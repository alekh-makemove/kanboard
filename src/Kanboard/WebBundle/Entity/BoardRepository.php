<?php
namespace Kanboard\WebBundle\Entity;

/**
 * Class BoardRepository
 * @package Kanboard\WebBundle\Entity
 */
class BoardRepository extends BaseRepository {

    /**
     * @param Board $board
     */
    public function save(Board $board) {
        $this->saveEntity($board);
    }

    /**
     * @param Board $board
     */
    public function remove(Board $board) {
        $this->removeEntity($board);
    }

}
