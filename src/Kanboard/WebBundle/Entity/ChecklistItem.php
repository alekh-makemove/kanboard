<?php

namespace Kanboard\WebBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * ChecklistItem
 *
 * @ORM\Table("checklists_items")
 * @ORM\Entity()
 */
class ChecklistItem {

    /**
     * @var integer

     * @ORM\Id
     * @ORM\Column(name="id", type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Checklist
     *
     * @ORM\ManyToOne(targetEntity="Checklist")
     * @ORM\JoinColumn(name="checklist_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $checklist;

    /**
     * @var String
     *
     * @ORM\Column(name="description", type="text")
     * @Assert\NotBlank()
     */
    private $description;

    /**
     * @var Boolean
     *
     * @ORM\Column(name="done", type="boolean")
     */
    private $done;

    /**
     * ChecklistItem constructor.
     * @param Checklist $checklist
     */
    public function __construct(Checklist $checklist) {
        $this->checklist = $checklist;
        $this->done = false;
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return Checklist
     */
    public function getChecklist() {
        return $this->checklist;
    }

    /**
     * @param Checklist $checklist
     */
    public function setChecklist($checklist) {
        $this->checklist = $checklist;
    }

    /**
     * @return String
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @param String $description
     */
    public function setDescription($description) {
        $this->description = $description;
    }

    /**
     * @return boolean
     */
    public function isDone() {
        return $this->done;
    }

    /**
     * @param boolean $done
     */
    public function setDone($done) {
        $this->done = $done;
    }

}