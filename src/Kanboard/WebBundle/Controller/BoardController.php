<?php
namespace Kanboard\WebBundle\Controller;

use Kanboard\WebBundle\Entity\Board;
use Kanboard\WebBundle\Entity\BoardRepository;
use Kanboard\WebBundle\Entity\ColumnRepository;
use Kanboard\WebBundle\Form\BoardType;
use Kanboard\WebBundle\Form\UserBoardType;
use Kanboard\WebBundle\Service\BoardService;
use Kanboard\WebBundle\Service\UserService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class BoardController
 *
 * @Route("/board")
 */
class BoardController extends BaseController {

    /**
     * @var BoardService
     * @DI\Inject("kb.board")
     */
    private $boardService;

    /**
     * @var BoardRepository
     * @DI\Inject("kb.repository.board")
     */
    private $boardRepository;

    /**
     * @var ColumnRepository
     * @DI\Inject("kb.repository.column")
     */
    private $columnRepository;

    /**
     * @var UserService
     * @DI\Inject("kb.user")
     */
    private $userService;

    /**
     * @Route("/", name="user_boards")
     * @Method("GET")
     * @Template()
     */
    public function userBoardsAction() {
        $currentUser = $this->getUser();
        $response = [
            'ownBoards' => [],
            'otherBoards' => []
        ];

        $boards = $this->getUser()->getBoards();
        foreach($boards as $board) {
            if($board->getCreator() == $currentUser) {
                $response['ownBoards'][] = $board;
            } else {
                $response['otherBoards'][] = $board;
            }
        }

        return $response;
    }

    /**
     * @Route("/create", name="board_create")
     * @Template("KanboardWebBundle:Board:boardForm.html.twig")
     */
    public function createBoardAction(Request $request) {
        $currentUser = $this->getUser();
        $board = new Board($currentUser);
        $form = $this->createForm(BoardType::class, $board);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $this->boardRepository->save($board);

            $flashMessage = $this->get('session')->getFlashBag();
            $flashMessage->add('notice', 'Board was successfully created.');

            $response = $this->redirectToRoute('user_boards');
        } else {
            $response = ['form' => $form->createView()];
        }

        return $response;
    }

    /**
     * @Route("/{boardId}/members", name="board_members", requirements={"boardId"="\d+$"})
     * @Template()
     * @Method("GET")
     */
    public function boardMembersAction($boardId) {
        $board = $this->boardService->getBoardByUserAndId($this->getUser(), $boardId);

        return ['members' => $board->getMembers()];
    }

    /**
     * @Route("/{boardId}/addMember", name="board_add_member", requirements={"boardId"="\d+$"})
     * @Template()
     */
    public function addMemberAction(Request $request, $boardId) {
        $form = $this->createForm(UserBoardType::class);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $email = $form->get('email')->getData();
            // Get board
            $board = $this->boardService->getBoardByCreatorAndId($this->getUser(), $boardId);
            // Get invited user
            $newMember = $this->userService->getUserByParams(['email' => $email]);
            // Add member
            if(!$board->getMembers()->contains($newMember)) {
                $board->getMembers()->add($newMember);
                $this->boardRepository->save($board);
            }

            $flashMessage = $this->get('session')->getFlashBag();
            $flashMessage->add('notice', 'Member was successfully added.');

            $response = $this->redirectToRoute('board_members', ['boardId' => $boardId]);
        } else {
            $response = ['form' => $form->createView()];
        }

        return $response;
    }

    /**
     * @Route("/{boardId}/disableMember/{userId}", name="board_disable_member", requirements={"boardId"="\d+$", "userId"="\d+$"})
     * @Template()
     */
    public function disableMemberAction($boardId, $userId) {
        // Get board
        $board = $this->boardService->getBoardByCreatorAndId($this->getUser(), $boardId);
        // Get member
        $member = $this->userService->getUserByParams(['id' => $userId]);

        if($board->getMembers()->contains($member)) {
            $board->getMembers()->removeElement($member);
            $this->boardRepository->save($board);
        }

        $flashMessage = $this->get('session')->getFlashBag();
        $flashMessage->add('notice', 'Member was successfully disabled.');

        return $this->redirectToRoute('board_members', ['boardId' => $boardId]);
    }

    /**
     * @Route("/{boardId}", name="board_columns", requirements={"boardId"="\d+$"})
     * @Method("GET")
     * @Template()
     */
    public function boardColumnsAction($boardId) {
        $board = $this->boardService->getBoardByUserAndId($this->getUser(), $boardId);
        $columns = $this->columnRepository->findBy(['board' => $board]);

        return ['columns' => $columns];
    }

    /**
     * @Route("/{boardId}/edit", name="edit_board", requirements={"boardId"="\d+$"})
     * @Template("KanboardWebBundle:Board:boardForm.html.twig")
     */
    public function editBoardAction(Request $request, $boardId) {
        $board = $this->boardService->getBoardByCreatorAndId($this->getUser(), $boardId);

        $form = $this->createForm(BoardType::class, $board);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $this->boardRepository->save($board);

            $flashMessage = $this->get('session')->getFlashBag();
            $flashMessage->add('notice', 'Board was successfully saved.');

            $response = $this->redirectToRoute('user_boards');
        } else {
            $response = ['form' => $form->createView()];
        }

        return $response;
    }

    /**
     * @Route("/{boardId}/delete", name="delete_board", requirements={"boardId"="\d+$"})
     */
    public function deleteBoardAction($boardId) {
        $board = $this->boardService->getBoardByCreatorAndId($this->getUser(), $boardId);
        $this->boardRepository->remove($board);

        $flashMessage = $this->get('session')->getFlashBag();
        $flashMessage->add('notice', 'Board was successfully deleted.');

        return $this->redirectToRoute('user_boards');
    }

}