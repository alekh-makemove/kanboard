<?php
namespace Kanboard\WebBundle\Controller;

use Kanboard\WebBundle\Entity\Checklist;
use Kanboard\WebBundle\Entity\ChecklistItem;
use Kanboard\WebBundle\Entity\ChecklistRepository;
use Kanboard\WebBundle\Form\ChecklistItemType;
use Kanboard\WebBundle\Form\ChecklistType;
use Kanboard\WebBundle\Service\CardService;
use Kanboard\WebBundle\Service\ChecklistService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class ChecklistController
 *
 * @Route("/checklist")
 */
class ChecklistController extends BaseController {

    /**
     * @var ChecklistService
     * @DI\Inject("kb.checklist")
     */
    private $checklistService;

    /**
     * @var CardService
     * @DI\Inject("kb.card")
     */
    private $cardService;

    /**
     * @var ChecklistRepository
     * @DI\Inject("kb.repository.checklist")
     */
    private $checklistRepository;

    /**
     * @Route("/create/{cardId}", name="create_checklist", requirements={"cardId"="\d+$"})
     * @Template("KanboardWebBundle:Checklist:checklistForm.html.twig")
     */
    public function createChecklistAction(Request $request, $cardId) {
        $currentUser = $this->getUser();
        $card = $this->cardService->getCardByUserAndCardId($currentUser, $cardId);

        $checklist = new Checklist($currentUser, $card);
        $form = $this->createForm(ChecklistType::class, $checklist);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $this->checklistRepository->save($checklist);

            $flashMessage = $this->get('session')->getFlashBag();
            $flashMessage->add('notice', 'Checklist was successfully created.');

            $response = $this->redirectToRoute('edit_card', ['cardId' => $cardId]);
        } else {
            $response = ['form' => $form->createView()];
        }

        return $response;
    }

    /**
     * @Route("/{checklistId}/add/item", name="add_checklist_item", requirements={"checklistId"="\d+$"})
     * @Template("KanboardWebBundle:Checklist:checklistItemForm.html.twig")
     */
    public function addChecklistItemAction(Request $request, $checklistId) {
        $checklist = $this->checklistService->getChecklistByUserAndId($this->getUser(), $checklistId);

        $checklistItem = new ChecklistItem($checklist);
        $form = $this->createForm(ChecklistItemType::class, $checklistItem);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $this->checklistRepository->saveChecklistItem($checklistItem);

            $flashMessage = $this->get('session')->getFlashBag();
            $flashMessage->add('notice', 'Item was successfully added.');

            $response = $this->redirectToRoute('edit_card', ['cardId' => $checklist->getCard()->getId()]);
        } else {
            $response = ['form' => $form->createView()];
        }

        return $response;
    }

    /**
     * @Route("/edit/item/{itemId}", name="edit_checklist_item", requirements={"itemId"="\d+$"})
     * @Template("KanboardWebBundle:Checklist:checklistItemForm.html.twig")
     */
    public function editChecklistItemAction(Request $request, $itemId) {
        $checklistItem = $this->checklistService->getChecklistItemByUserAndId($this->getUser(), $itemId);

        $form = $this->createForm(ChecklistItemType::class, $checklistItem);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $this->checklistRepository->saveChecklistItem($checklistItem);

            $flashMessage = $this->get('session')->getFlashBag();
            $flashMessage->add('notice', 'Item was successfully saved.');

            $response = $this->redirectToRoute('edit_card', ['cardId' => $checklistItem->getChecklist()->getCard()->getId()]);
        } else {
            $response = ['form' => $form->createView()];
        }

        return $response;
    }

    /**
     * @Route("/delete/item/{itemId}", name="delete_checklist_item", requirements={"itemId"="\d+$"})
     */
    public function deleteChecklistItemAction($itemId) {
        $checklistItem = $this->checklistService->getChecklistItemByUserAndId($this->getUser(), $itemId);
        $cardId = $checklistItem->getChecklist()->getCard()->getId();
        $this->checklistRepository->removeChecklistItem($checklistItem);

        $flashMessage = $this->get('session')->getFlashBag();
        $flashMessage->add('notice', 'Item was successfully deleted.');

        return $this->redirectToRoute('edit_card', ['cardId' => $cardId]);
    }

    /**
     * @Route("/delete/{checklistId}", name="delete_checklist", requirements={"checklistId"="\d+$"})
     */
    public function deleteChecklistAction($checklistId) {
        $checklist = $this->checklistService->getChecklistByUserAndId($this->getUser(), $checklistId);
        $cardId = $checklist->getCard()->getId();
        $this->checklistRepository->remove($checklist);

        $flashMessage = $this->get('session')->getFlashBag();
        $flashMessage->add('notice', 'Checklist was successfully deleted.');

        return $this->redirectToRoute('edit_card', ['cardId' => $cardId]);
    }

}