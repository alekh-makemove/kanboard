<?php
namespace Kanboard\WebBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MainController
 *
 * @Route("/")
 */
class MainController extends BaseController {

    /**
     * @Route("/", name="home")
     * @Method({"GET"})
     * @Template()
     */
    public function homeAction() {
        return [];
    }

}