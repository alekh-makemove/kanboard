<?php
namespace Kanboard\WebBundle\Controller;

use Kanboard\WebBundle\Entity\Card;
use Kanboard\WebBundle\Entity\CardComment;
use Kanboard\WebBundle\Entity\CardRepository;
use Kanboard\WebBundle\Form\CardCommentType;
use Kanboard\WebBundle\Form\CardType;
use Kanboard\WebBundle\Service\CardService;
use Kanboard\WebBundle\Service\ColumnService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class CardController
 *
 * @Route("/card")
 */
class CardController extends BaseController {

    /**
     * @var ColumnService
     * @DI\Inject("kb.column")
     */
    private $columnService;

    /**
     * @var CardService
     * @DI\Inject("kb.card")
     */
    private $cardService;

    /**
     * @var CardRepository
     * @DI\Inject("kb.repository.card")
     */
    private $cardRepository;

    /**
     * @Route("/add/{columnId}", name="add_card", requirements={"columnId"="\d+$"})
     * @Template("KanboardWebBundle:Card:cardForm.html.twig")
     */
    public function addAction(Request $request, $columnId) {
        $column = $this->columnService->getColumnByUserAndId($this->getUser(), $columnId);
        $board = $column->getBoard();

        $card = new Card($column);
        $form = $this->createForm(CardType::class, $card, [
            'boardMembers' => $board->getMembers()
        ]);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $this->cardRepository->save($card);

            $flashMessage = $this->get('session')->getFlashBag();
            $flashMessage->add('notice', 'Card was successfully added.');

            $response = $this->redirectToRoute('board_columns', ['boardId' => $board->getId()]);
        } else {
            $response = [
                'form' => $form->createView(),
                'card' => $card
            ];
        }

        return $response;
    }

    /**
     * @Route("/edit/{cardId}", name="edit_card", requirements={"cardId"="\d+$"})
     * @Template("KanboardWebBundle:Card:cardForm.html.twig")
     */
    public function editAction(Request $request, $cardId) {
        $card = $this->cardService->getCardByUserAndCardId($this->getUser(), $cardId);
        $board = $card->getColumn()->getBoard();

        $form = $this->createForm(CardType::class, $card, [
            'boardMembers' => $board->getMembers()
        ]);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $this->cardRepository->save($card);

            $flashMessage = $this->get('session')->getFlashBag();
            $flashMessage->add('notice', 'Card was successfully saved.');

            $response = $this->redirectToRoute('board_columns', ['boardId' => $board->getId()]);
        } else {
            $response = [
                'form' => $form->createView(),
                'card' => $card
            ];
        }

        return $response;
    }

    /**
     * @Route("/delete/{cardId}", name="delete_card", requirements={"cardId"="\d+$"})
     */
    public function deleteAction($cardId) {
        $card = $this->cardService->getCardByUserAndCardId($this->getUser(), $cardId);
        $boardId = $card->getColumn()->getBoard()->getId();

        $this->cardRepository->remove($card);

        $flashMessage = $this->get('session')->getFlashBag();
        $flashMessage->add('notice', 'Card was successfully deleted.');

        return $this->redirectToRoute('board_columns', ['boardId' => $boardId]);
    }

    /**
     * @Route("/{cardId}/comment/add", name="card_add_comment", requirements={"cardId"="\d+$"})
     * @Template("KanboardWebBundle:Card:commentForm.html.twig")
     */
    public function addCommentAction(Request $request, $cardId) {
        $currentUser = $this->getUser();
        $card = $this->cardService->getCardByUserAndCardId($currentUser, $cardId);

        $comment = new CardComment($currentUser, $card);
        $form = $this->createForm(CardCommentType::class, $comment);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $this->cardRepository->saveComment($comment);

            $flashMessage = $this->get('session')->getFlashBag();
            $flashMessage->add('notice', 'Comment was successfully added.');

            $response = $this->redirectToRoute('edit_card', ['cardId' => $cardId]);
        } else {
            $response = ['form' => $form->createView()];
        }

        return $response;
    }

}