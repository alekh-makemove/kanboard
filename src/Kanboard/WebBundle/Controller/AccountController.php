<?php
namespace Kanboard\WebBundle\Controller;

use Kanboard\WebBundle\Entity\User;
use Kanboard\WebBundle\Entity\UserRepository;
use Kanboard\WebBundle\Form\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class AccountController
 */
class AccountController extends BaseController {

    /**
     * @var UserRepository
     * @DI\Inject("kb.repository.user")
     */
    private $userRepository;

    /**
     * @Route("registration", name="registration")
     * @Template()
     */
    public function registrationAction(Request $request) {
        $currentUser = $this->getUser();
        if ($currentUser != null) {
            $response = $this->redirectToRoute('user_boards');
        } else {
            $newUser = new User();
            $form = $this->createForm(UserType::class, $newUser);

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $password = $this
                    ->get('security.password_encoder')
                    ->encodePassword($newUser, $newUser->getPlainPassword());
                $newUser->setPassword($password);
                $this->userRepository->save($newUser);

                $response = $this->redirectToRoute('user_boards');
            } else {
                $response = ['form' => $form->createView()];
            }
        }

        return $response;
    }

    /**
     * @Route("/login", name="login")
     * @Template()
     */
    public function loginAction() {
        $user = $this->getUser();
        $response = [];
        if ($user != null) {
            $response = $this->redirectToRoute('user_boards');
        } else {
            $authenticationUtils = $this->get('security.authentication_utils');
            $error = $authenticationUtils->getLastAuthenticationError();

            if ($error !== null && $error instanceof \Exception) {
                $flashMessage = $this->get('session')->getFlashBag();
                $flashMessage->add('notice', $error->getMessage());
            }
        }

        return $response;
    }

}