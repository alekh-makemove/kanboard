<?php
namespace Kanboard\WebBundle\Controller;

use Kanboard\WebBundle\Entity\Column;
use Kanboard\WebBundle\Entity\ColumnRepository;
use Kanboard\WebBundle\Form\ColumnType;
use Kanboard\WebBundle\Service\BoardService;
use Kanboard\WebBundle\Service\ColumnService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class ColumnController
 *
 * @Route("/column")
 */
class ColumnController extends BaseController {

    /**
     * @var ColumnService
     * @DI\Inject("kb.column")
     */
    private $columnService;

    /**
     * @var ColumnRepository
     * @DI\Inject("kb.repository.column")
     */
    private $columnRepository;

    /**
     * @var BoardService
     * @DI\Inject("kb.board")
     */
    private $boardService;

    /**
     * @Route("/create/{boardId}", name="create_column", requirements={"boardId"="\d+$"})
     * @Template("KanboardWebBundle:Column:columnForm.html.twig")
     */
    public function createColumnAction(Request $request, $boardId) {
        $board = $this->boardService->getBoardByUserAndId($this->getUser(), $boardId);

        $column = new Column($board);
        $form = $this->createForm(ColumnType::class, $column);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $this->columnRepository->save($column);

            $flashMessage = $this->get('session')->getFlashBag();
            $flashMessage->add('notice', 'Column was successfully created.');

            $response = $this->redirectToRoute('board_columns', ['boardId' => $boardId]);
        } else {
            $response = ['form' => $form->createView()];
        }

        return $response;
    }

    /**
     * @Route("/{columnId}/edit", name="edit_column", requirements={"columnId"="\d+$"})
     * @Template("KanboardWebBundle:Column:columnForm.html.twig")
     */
    public function editColumnAction(Request $request, $columnId) {
        $column = $this->columnService->getColumnByUserAndId($this->getUser(), $columnId);

        $form = $this->createForm(ColumnType::class, $column);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $this->columnRepository->save($column);

            $flashMessage = $this->get('session')->getFlashBag();
            $flashMessage->add('notice', 'Column was successfully saved.');

            $response = $this->redirectToRoute('board_columns', ['boardId' => $column->getBoard()->getId()]);
        } else {
            $response = ['form' => $form->createView()];
        }

        return $response;
    }

    /**
     * @Route("/{columnId}/delete", name="delete_column", requirements={"columnId"="\d+$"})
     */
    public function deleteBoardAction($columnId) {
        $column = $this->columnService->getColumnByUserAndId($this->getUser(), $columnId);
        $this->columnService->deleteColumn($column);

        $flashMessage = $this->get('session')->getFlashBag();
        $flashMessage->add('notice', 'Column was successfully deleted.');

        return $this->redirectToRoute('board_columns', ['boardId' => $column->getBoard()->getId()]);
    }

}