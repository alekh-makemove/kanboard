<?php
namespace Kanboard\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class BaseController
 * @package Kanboard\WebBundle\Controller
 */
class BaseController extends Controller {

}