<?php
namespace Kanboard\WebBundle\Twig;

use Kanboard\WebBundle\Helper\TimeTranslatorHelper as TT;

/**
 * Class KanboardTwigExtension
 * @package Kanboard\WebBundle\Twig
 */
class KanboardTwigExtension extends \Twig_Extension {

    const REMAINING_TYPE = 0;
    const ESTIMATE_TYPE = 1;

    /**
     * @return array
     */
    public function getFilters() {
        return [
            new \Twig_SimpleFilter('remainingTimeSummer', [$this, 'remainingTimeSummer']),
            new \Twig_SimpleFilter('estimateTimeSummer', [$this, 'estimateTimeSummer'])
        ];
    }

    /**
     * @param $items
     * @return string
     */
    public function remainingTimeSummer($items) {
        return $this->timeSummer($items, self::REMAINING_TYPE);
    }

    /**
     * @param $items
     * @return string
     */
    public function estimateTimeSummer($items) {
        return $this->timeSummer($items, self::ESTIMATE_TYPE);
    }

    /**
     * @param $items
     * @param int $type
     * @return string
     */
    private function timeSummer($items, $type = self::ESTIMATE_TYPE) {
        $time = 0;
        foreach ($items as $item) {
            switch ($type) {
                case (self::ESTIMATE_TYPE):
                    $time += $item->getEstimateTimeInMinutes();
                    break;

                case (self::REMAINING_TYPE):
                    $time += $item->getRemainingTimeInMinutes();
                    break;
            }
        }

        return TT::timeToString($time);
    }

    /**
     * @return string
     */
    public function getName() {
        return 'kanboard_twig_extension';
    }
}