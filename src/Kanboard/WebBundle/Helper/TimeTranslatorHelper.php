<?php
namespace Kanboard\WebBundle\Helper;

/**
 * Class TimeTranslatorHelper
 * @package Kanboard\WebBundle\Helper
 */
class TimeTranslatorHelper {

    /**
     * Convert string like '1d 13h 24m' to minutes
     *
     * @param $timeString
     * @return int
     */
    static public function timeToMinutes($timeString) {
        $result = 0;
        if (preg_match('/^(?:(?<days>\d+)d\s*)?(?:(?<hours>\d+)h\s*)?(?:(?<minutes>\d+)m\s*)?$/', $timeString, $matches)) {
            $days = $matches['days'] ?? 0;
            $hours = $matches['hours'] ?? 0;
            $minutes = $matches['minutes'] ?? 0;

            $result = ($days * 24 + $hours) * 60 + $minutes;
        }

        return $result;
    }

    /**
     * Convert minutes to string like '1d 13h 24m'
     *
     * @param $minutes
     * @return string
     */
    static public function timeToString($minutes) {
        $days = floor($minutes / 1440);
        $hours = floor(($minutes % 1440) / 60);
        $minutes = $minutes % 60;

        $result =
            ($days != 0 ? $days . 'd ' : '') .
            ($hours != 0 ? $hours . 'h ' : '') .
            ($minutes != 0 ? $minutes . 'm' : '');

        return trim($result);
    }

}