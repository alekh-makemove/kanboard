<?php
namespace Kanboard\WebBundle\Helper;

/**
 * Class StringUtils
 * @package Kanboard\WebBundle\Helper
 */
class StringUtils {

    /**
     * @param ...$strings
     * @return bool
     */
    public static function isBlankOrNull(...$strings) {
        $result = false;
        foreach ($strings as $string) {
            if ($string === null || trim($string) === '') {
                $result = true;
                break;
            }
        }

        return $result;
    }

}
