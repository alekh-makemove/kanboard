<?php
namespace Kanboard\WebBundle\Service;

use Kanboard\WebBundle\Entity\ChecklistRepository;
use Kanboard\WebBundle\Entity\User;
use JMS\DiExtraBundle\Annotation as DI;
use Kanboard\WebBundle\KanboardException;

/**
 * Class UserService
 * @DI\Service("kb.checklist")
 */
class ChecklistService {

    /**
     * @var ChecklistRepository
     */
    public $checklistRepository;

    /**
     * @var BoardService
     */
    public $boardService;

    /**
     * @DI\InjectParams({
     *     "checklistRepository" = @DI\Inject("kb.repository.checklist"),
     *     "boardService" = @DI\Inject("kb.board")
     * })
     */
    public function setUserRepository(ChecklistRepository $checklistRepository, BoardService $boardService) {
        $this->checklistRepository = $checklistRepository;
        $this->boardService = $boardService;
    }

    /**
     * @param User $user
     * @param $id
     * @return null|object
     * @throws KanboardException
     */
    public function getChecklistByUserAndId(User $user, $id) {
        $checklist = $this->checklistRepository->find($id);
        $board = $checklist->getCard()->getColumn()->getBoard();
        if (!$this->boardService->hasPermission($user, $board)) {
            throw new KanboardException("You don't have permission.");
        }

        return $checklist;
    }

    /**
     * @param User $user
     * @param $id
     * @return mixed
     * @throws KanboardException
     */
    public function getChecklistItemByUserAndId(User $user, $id) {
        $checklistItem = $this->checklistRepository->getChecklistItem($id);
        $board = $checklistItem->getChecklist()->getCard()->getColumn()->getBoard();
        if (!$this->boardService->hasPermission($user, $board)) {
            throw new KanboardException("You don't have permission.");
        }

        return $checklistItem;
    }

}