<?php
namespace Kanboard\WebBundle\Service;

use Kanboard\WebBundle\Entity\User;
use JMS\DiExtraBundle\Annotation as DI;
use Kanboard\WebBundle\Entity\UserRepository;
use Kanboard\WebBundle\KanboardException;

/**
 * Class UserService
 * @DI\Service("kb.user")
 */
class UserService {

    /**
     * @var UserRepository
     */
    public $userRepository;

    /**
     * @DI\InjectParams({
     *     "userRepository" = @DI\Inject("kb.repository.user")
     * })
     */
    public function setUserRepository(UserRepository $userRepository) {
        $this->userRepository = $userRepository;
    }

    /**
     * @param array $params
     * @return null|User
     * @throws KanboardException
     */
    public function getUserByParams(array $params) {
        $user = $this->userRepository->findOneBy($params);
        if ($user == null) {
            throw new KanboardException('User not found.');
        }

        return $user;
    }

}