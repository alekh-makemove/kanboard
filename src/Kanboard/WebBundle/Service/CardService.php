<?php
namespace Kanboard\WebBundle\Service;

use Kanboard\WebBundle\Entity\CardRepository;
use Kanboard\WebBundle\Entity\User;
use JMS\DiExtraBundle\Annotation as DI;
use Kanboard\WebBundle\KanboardException;

/**
 * Class CardService
 * @DI\Service("kb.card")
 */
class CardService {

    /**
     * @var CardRepository
     */
    public $cardRepository;

    /**
     * @var BoardService
     */
    public $boardService;

    /**
     * @DI\InjectParams({
     *     "cardRepository" = @DI\Inject("kb.repository.card"),
     *     "boardService" = @DI\Inject("kb.board")
     * })
     */
    public function __controller(CardRepository $cardRepository, BoardService $boardService) {
        $this->cardRepository = $cardRepository;
        $this->boardService = $boardService;
    }

    /**
     * @param User $user
     * @param $cardId
     * @return null|object
     * @throws KanboardException
     */
    public function getCardByUserAndCardId(User $user, $cardId) {
        $card = $this->cardRepository->findOneBy(['id' => $cardId]);
        if ($card == null) {
            throw new KanboardException('Card not found.');
        }

        $board = $card->getColumn()->getBoard();
        if (!$this->boardService->hasPermission($user, $board)) {
            throw new KanboardException("You don't have permission.");
        }

        return $card;
    }

}