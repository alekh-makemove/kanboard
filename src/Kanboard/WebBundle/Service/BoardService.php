<?php
namespace Kanboard\WebBundle\Service;

use Kanboard\WebBundle\Entity\Board;
use Kanboard\WebBundle\Entity\BoardRepository;
use Kanboard\WebBundle\Entity\User;
use JMS\DiExtraBundle\Annotation as DI;
use Kanboard\WebBundle\KanboardException;

/**
 * Class BoardService
 * @DI\Service("kb.board")
 */
class BoardService {

    /**
     * @var BoardRepository
     */
    public $boardRepository;

    /**
     * @DI\InjectParams({
     *     "boardRepository" = @DI\Inject("kb.repository.board")
     * })
     */
    public function __controller(BoardRepository $boardRepository) {
        $this->boardRepository = $boardRepository;
    }

    /**
     * @param User $user
     * @param $boardId
     * @return mixed
     * @throws KanboardException
     */
    public function getBoardByUserAndId(User $user, $boardId) {
        $board = $this->boardRepository->findOneBy(['id' => $boardId]);
        if ($board == null) {
            throw new KanboardException('Board not found.');
        }

        if(!$this->hasPermission($user, $board)) {
            throw new KanboardException("You don't have permission.");
        }

        return $board;
    }

    /**
     * @param User $user
     * @param $boardId
     * @return mixed
     * @throws KanboardException
     */
    public function getBoardByCreatorAndId(User $user, $boardId) {
        $board = $this->boardRepository->findOneBy(['id' => $boardId, 'creator' => $user]);
        if ($board == null) {
            throw new KanboardException('Board not found.');
        }

        return $board;
    }

    /**
     * @param User $user
     * @param Board $board
     * @return mixed
     */
    public function hasPermission(User $user, Board $board) {
        return $board->getMembers()->contains($user);
    }

}