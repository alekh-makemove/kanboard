<?php
namespace Kanboard\WebBundle\Service;

use Kanboard\WebBundle\Entity\Column;
use Kanboard\WebBundle\Entity\ColumnRepository;
use Kanboard\WebBundle\Entity\User;
use JMS\DiExtraBundle\Annotation as DI;
use Kanboard\WebBundle\KanboardException;

/**
 * Class ColumnService
 * @DI\Service("kb.column")
 */
class ColumnService {

    /**
     * @var ColumnRepository
     */
    public $columnRepository;

    /**
     * @var BoardService
     */
    public $boardService;

    /**
     * @DI\InjectParams({
     *     "columnRepository" = @DI\Inject("kb.repository.column"),
     *     "boardService" = @DI\Inject("kb.board")
     * })
     */
    public function __controller(ColumnRepository $columnRepository, BoardService $boardService) {
        $this->columnRepository = $columnRepository;
        $this->boardService = $boardService;
    }

    /**
     * @param User $user
     * @param $columndId
     * @return mixed
     * @throws KanboardException
     */
    public function getColumnByUserAndId(User $user, $columndId) {
        $column = $this->columnRepository->findOneBy(['id' => $columndId]);
        if ($column == null) {
            throw new KanboardException('Column not found.');
        }

        if(!$this->boardService->hasPermission($user, $column->getBoard())) {
            throw new KanboardException("You don't have permission.");
        }

        return $column;
    }

    /**
     * @param Column $column
     */
    public function deleteColumn(Column $column) {
        $this->columnRepository->remove($column);
    }

}