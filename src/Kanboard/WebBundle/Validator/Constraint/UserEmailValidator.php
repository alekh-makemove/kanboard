<?php
namespace Kanboard\WebBundle\Validator\Constraint;

use Kanboard\WebBundle\Entity\UserRepository;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UserEmailValidator extends ConstraintValidator {

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UserEmailValidator constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository) {
        $this->userRepository = $userRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint) {
        if (!$constraint instanceof UserEmail) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\UserEmail');
        }

        if ($this->userRepository->findOneBy(['email' => $value]) == null) {
            $this
                ->context
                ->buildViolation('User with this email not found.')
                ->addViolation();
        }
    }

}