<?php
namespace Kanboard\WebBundle\Validator\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class UserEmail extends Constraint {
    public $message = 'User with this email not found.';
}