<?php

namespace Kanboard\WebBundle\Security;

use Kanboard\WebBundle\Entity\User;
use Kanboard\WebBundle\Entity\UserRepository;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * @DI\Service("kb.security.user.provider")
 */
class UserProvider implements UserProviderInterface {

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @DI\InjectParams({
     *     "userRepository" = @DI\Inject("kb.repository.user")
     * })
     */
    public function __construct(UserRepository $userRepository) {
        $this->userRepository = $userRepository;
    }

    /**
     * @param string $username
     */
    public function loadUserByUsername($username) {

    }

    /**
     * @param $email
     * @return mixed
     */
    public function loadUserByEmail($email) {
        $user = $this->userRepository->findUserByEmail($email);
        if ($user == null) {
            throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $email));
        }

        return $user;
    }

    /**
     * @param UserInterface $user
     * @return mixed
     */
    public function refreshUser(UserInterface $user) {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        return $this->loadUserByEmail($user->getEmail());
    }

    /**
     * @param string $class
     * @return bool
     */
    public function supportsClass($class) {
        return $class === 'Kanboard\WebBundle\Entity\User';
    }
}