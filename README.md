# README #

### What is this repository for? ###

Kanboard

This project is a kanban board implementation.

### How do I get set up? ###

1. Clone this repository
2. Run composer install for install dependencies and project parameters:
> $ composer install
3. Run next command for create database:
> $ php bin/console doctrine:database:create
>
> $ php bin/console doctrine:schema:update --force
4. You will need create session table in youe DB manualy:
> DB schema:
>
> http://symfony.com/doc/current/cookbook/doctrine/pdo_session_storage.html#mysql
5. Also you will need execute next commands for install all asserts:
> $ php bin/console assets:install
>
> $ php bin/console assetic:dump
6. Create virtual host for project.
> Virtual host configurations:
>
> http://symfony.com/doc/current/cookbook/configuration/web_server_configuration.html

### Usage ###

In this service you will be able to register and login as a regular user.
You also will be able to create your own kanban boards, and manage them.
Boards have members and columns with cards.
All members can edit column and cards.